﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_07.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            //====Table Size======
            //report.Width = Unit.Pixel(1100);
            report.SizeToReportContent = true;
            report.AsyncRendering = true;

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("userinput", "john");

            report.ServerReport.ReportPath = "/Person";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
           
            ViewBag.ReportViewer = report;
            return View();
        }
        
        public ActionResult FirstNameParam(string fname)
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;

            //report.Width = Unit.Pixel(1100);
            report.SizeToReportContent = true;
            report.AsyncRendering = true;

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/Person";

            ReportParameter param = new ReportParameter();
            param.Name = "userinput";
            param.Values.Add(fname);

            report.ServerReport.SetParameters(param);

            ViewBag.ReportViewer = report;
            return View();
        }
    }
}